import {convertToMultibase } from "./utils.js";

export function getController(req) {
    if (process.env.NODE_ENV == 'test') { // TODO; use sinon.js instead
        return 'http://' + req.get('host') + '/issuer/did.json';
    } else {
        return 'did:web:' + req.get('host').replace(':', encodeURIComponent(':')) + ':issuer';
    }
}

export const getDID = async (config, req) => {
    const rsapubjwk = config.cert.domain.certificate.publicKey.export({ format: 'jwk', type: 'pkcs1' });
    const ed25519pubjwk = config.cert.issuer.publicKey.export({ format: 'jwk', type: 'spki' });
    const ed25519pubMultibase = convertToMultibase(config.cert.issuer.publicKey);

    const controller = getController(req);
    let did = {
        "@context": [
            "https://www.w3.org/ns/did/v1"
        ],
        id: controller,
        controller: controller,
        verificationMethod: [
            {
                '@context': 'https://w3id.org/security/suites/jws-2020/v1',
                id: controller + "#JsonWebKey2020-" + ed25519pubjwk.crv, // kty == OKP https://datatracker.ietf.org/doc/html/rfc8037#section-2
                controller: controller,
                type: "JsonWebKey2020", // https://www.w3.org/TR/did-spec-registries/
                publicKeyJwk: ed25519pubjwk
            },
            {
                '@context': 'https://w3id.org/security/suites/ed25519-2020/v1',
                id: controller + "#Ed25519VerificationKey2020",
                controller: controller,
                type: "Ed25519VerificationKey2020",
                publicKeyMultibase: ed25519pubMultibase
            },
            {
                '@context': 'https://w3id.org/security/suites/jws-2020/v1',
                id: controller + "#JsonWebKey2020-" + rsapubjwk.kty,
                controller: controller,
                type: "JsonWebKey2020",
                publicKeyJwk: rsapubjwk
            }
        ],
        assertionMethod: [
        ],
        service: [{
            id: controller + "#svc-fullchain",
            type: "X509Certificate",
            serviceEndpoint: req.protocol + "://" + req.get('host') + '/issuer/fullchain.cer'
        }]
    }

    for (let method of did.verificationMethod) {
        did.assertionMethod.push(method.id);
    }
    
    return did;
};
