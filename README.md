# Gaia-X VC issuer

## Purpose

The goal of this service is to issue [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/) about:
- Gaia-X association membership 
- Gaia-X Working Group membership 

The purpose is to later use those Verifiable Credentials to access data or service that require access control. The access will be controlled using attributes from the Verifiable Credentials.

### Full scenario

The intended usecase would be that:
1. As a Gaia-X participant, I browse to https://vc.gaia-x.eu/membership where the user finds a form+captcha to submit his/her email.
2. The service sends a message to the email to verify that the user really has control over the provided email (standard process "please click here to validate that you are the email's owner and initial requester for Gaia-X membership VC issuance").
3. Once the email is verified, we ask to connect to a wallet, like [MetaMask](https://docs.metamask.io/guide/signing-data.html) to sign the claim about email ownership with user's wallet key.
4. The service also generates a Verifiable Credential about membership signed with Gaia-X key.
5. The service returns back an array of Verifiable Credential
    - claim about email and wallet address link, signed by the user
    - claim about email and membership link, signed by the Gaia-X service
6. We have to find a solution to store W3C Verifiable Credentials, either using a mobile app or a browser extension.

Then the user can use this Verifiable Credentials to access other resources as a Gaia-x member.  
The 3rd party website will be able to verify both claim
- It's issued by Gaia-X service
- The holder is really to one who confirmed his email using MetaMask extension

```mermaid
sequenceDiagram
    autonumber
    participant wallet as User's wallet<br>(MetaMask)
    actor user as User
    participant service as VC issuers service
        
%%rect rgba(242, 242, 230, 1)
    Note over wallet, service: Step1: validates email
    user ->> service: browses and fills form with email + captcha
    service -->> user: asks to validate email
    user -->> service: validates email
%%end
    Note over wallet, service: Step2: generate VC
    service ->> user: asks to connect to a wallet
    user ->> wallet: opens wallet
    wallet -->> service: 
    service ->> service: checks in internal DB for membership
    service ->> user: asks to sign VC about Person(membership, email, walletAddress)
    user ->> wallet: approves signature
    wallet -->> service: 
    service ->> user: issues Verifiable Credential
```

## Challenge(s)

We want to adopt SSI concepts, giving the user (Holder) the control of its data aka attributes.

How the Verifier verifies that the Holder's attributes are issued by a properly known Issuer based on Issuer's X.509 certificates, with either:
- its RSA domain name SSL certificate
- its RSA eIDAS identity

![](https://www.w3.org/TR/vc-data-model/diagrams/ecosystem.svg)
<!-- .element style="border: 0; background: None; box-shadow: None" -->

## Getting started

The service is hosted on [clever-cloud.com](https://www.clever-cloud.com/) here <https://vc.gaia-x.eu/>

## Development

### Testing

```shell
npm test
```

### CI/CD

Config in [`.gitlab-ci-yml`](./-/blob/main/.gitlab-ci.yml)

## Immediate next steps

- [X] generate Let's Encrypt cert for the domain and put public key in the [W3C DID](https://www.w3.org/TR/did-core/) document
- [X] expose DID [`did:web:vc.gaia-x.eu:issuer`](https://vc.gaia-x.eu/issuer/did.json)
- [ ] generate swagger
